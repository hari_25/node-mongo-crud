const mongoose = require("mongoose");

mongoose
    .connect("mongodb://localhost:27017/hari", {
        useNewUrlParser: true,
    })
    .catch((e) => {
        console.log("Connection error");
    });
