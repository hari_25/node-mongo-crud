require("dotenv").config();

const os = require("os");
const path = require("path");
const express = require("express");
const formData = require("express-form-data");

require("./Config/MongoDB");

const EmployeeRoute = require("./src/Routes/EmployeeRoute");

const app = express();

const PORT = process.env.PORT || 4000;

const options = {
    uploadDir: os.tmpdir(),
    autoClean: true,
};

app.use(express.json());
app.use(express.static(path.join(__dirname, "/")));

// app.use(formData.parse(options));

app.use("/employee", EmployeeRoute);

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});
