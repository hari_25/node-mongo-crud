const multer = require("multer");

const storage = multer.diskStorage({
    destination: function (req, file, next) {
        next(null, "./public/uploads/");
    },
    filename: function (req, file, next) {
        next(null, Date.now() + "." + file.originalname.split(".").pop());
    },
});

const fileFilter = (req, file, next) => {
    if (["image/jpeg", "image/jpg", "image/png"].includes(file.mimetype)) {
        next(null, true);
    } else {
        next(null, false);
    }
};

module.exports = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5,
    },
    fileFilter: fileFilter,
});
