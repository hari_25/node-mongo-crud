const express = require("express");

const auth = require("../Middleware/Auth");
const { validator, messages } = require("../Errors/Employee");
const EmployeeController = require("../Controllers/EmployeeController");
const upload = require("../Helpers/FileUpload");
const test = require("../Helpers/Test");

const router = express.Router();

async function uploadDocuments(req, res, next) {
    logger.info("uploadDocuments Entry --> ");
    //logger.info("req*******", req);
    let rVal;
    let code = 200;
    //if no file is selected to upload
    if (!req.headers["content-type"]) {
        code = 500;
        rVal = "No file found";
        return responseHandler.sendResponse(
            res,
            "uploadDocuments",
            code,
            rVal,
            false,
        );
    }

    upload(req, res, function (err) {
        if (err) {
            rVal = "Failed to upload document.";
            code = 500;
            logger.error(
                "Failed to upload document to temp location.",
                err,
                "for data:",
                req.files,
            );
            return responseHandler.sendResponse(
                res,
                "uploadDocuments",
                code,
                rVal,
                false,
            );
        }
        let fileList;
        fileList = path.join(helper.getBasedir(), req.file.originalname);
        helper.setFileName(req.file.originalname);

        let fileNames = req.file.originalname;
        docService.storeDocument(fileList, fileNames).then(
            (success) => {
                rVal = "Succesfully uploadeded document";
                logger.info(rVal);
                // imagesList.forEach(imagepath => {
                let id = req.params.id + "_" + req.user._id;

                FilesList.fileDetails(id, fileNames).then(() => {
                    fs.unlink(fileList, function () {});
                });
                next();
            },
            (err) => {
                rVal = "Failed to upload document. Error: " + err;
                code = 500;
                logger.error(rVal);
                return responseHandler.sendResponse(
                    res,
                    "uploadDocuments",
                    code,
                    rVal,
                    false,
                );
            },
        );
    });
}

router.post(
    "/register",
    // uploadDocuments,
    upload.single("image"),
    [validator(), messages],
    EmployeeController.register,
);
router.post("/login", EmployeeController.login);
router.post("/list", auth, EmployeeController.list);
router.post("/profile", auth, EmployeeController.profile);
router.put("/update", auth, auth, EmployeeController.update);
router.delete("/delete", auth, EmployeeController.delete);
// router.post("/logout", auth, EmployeeController.logout);

module.exports = router;
