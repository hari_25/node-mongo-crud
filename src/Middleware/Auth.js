const jwt = require("jsonwebtoken");

const apiResponse = require("../Helpers/ApiResponse");

module.exports = (req, res, next) => {
    const token =
        req.body.token || req.query.token || req.headers["authorization"];

    if (!token) {
        const msg = "Token is required";
        return apiResponse.validationError(res, msg, []);
    }

    try {
        req.params = jwt.verify(token, "Node_Mongo_CRUD");
    } catch (err) {
        const msg = "Invalid Token";
        return apiResponse.unauthorizedResponse(res, msg, []);
    }

    return next();
};
