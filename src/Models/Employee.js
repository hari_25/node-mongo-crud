const mongoose = require("mongoose");

module.exports = mongoose.model(
    "Employee",
    new mongoose.Schema(
        {
            name: {
                type: String,
                required: true,
            },
            email: {
                type: String,
                required: true,
            },
            password: {
                type: String,
                required: true,
            },
            emp_code: {
                type: Number,
                required: true,
            },
            date_of_joining: {
                type: String,
                required: true,
            },
            salary: {
                type: Number,
                required: true,
            },
            gender: {
                type: Number,
                required: true,
            },
            address: {
                type: String,
            },
            image: {
                type: String,
            },
            token: {
                type: String,
            },
        },
        { versionKey: false },
    ),
);
