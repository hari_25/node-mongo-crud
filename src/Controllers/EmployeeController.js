const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const upload = require("../Helpers/FileUpload");
const EmployeeModal = require("../Models/Employee");
const apiResponse = require("../Helpers/ApiResponse");

module.exports.list = async (req, res) => {
    try {
        const employees = await EmployeeModal.find();
        const msg = employees.length ? "Employee found" : "Employee not found!";

        return apiResponse.successResponse(res, msg, employees);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

module.exports.profile = async (req, res) => {
    try {
        const employee = await EmployeeModal.findById(req.params._id);
        const msg = employee.length ? "Employee found" : "Employee not found!";

        return apiResponse.successResponse(res, msg, employee);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

module.exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        const employee = await EmployeeModal.findOne({ email: email });

        if (employee && (await bcrypt.compare(password, employee.password))) {
            employee.token = await jwt.sign(
                { _id: employee._id },
                "Node_Mongo_CRUD",
            );
        } else {
            return apiResponse.unauthorizedResponse(
                res,
                "Invalid credentials!",
                [],
            );
        }

        const msg = employee.length ? "Employee found" : "Employee not found!";

        return apiResponse.successResponse(res, msg, employee);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

module.exports.register = async (req, res) => {
    try {
        req.body.password = await bcrypt.hash(req.body.password, 10);
        req.body.emp_code = getRndInteger();
        req.body.image = req.file ? req.file.filename : null;

        const employee = await new EmployeeModal(req.body).save();
        const msg = "Employee added successfully";

        return apiResponse.successResponse(res, msg, employee);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

module.exports.update = async (req, res) => {
    try {
        const employee = await EmployeeModal.findByIdAndUpdate(
            req.params._id,
            req.body,
            { new: true },
        );
        const msg = "Employee updated successfully";

        return apiResponse.successResponse(res, msg, employee);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

// module.exports.logout = async (req, res) => {
//     try {
//         const msg = "Employee logout successfully";

//         jwt.destroy(req.headers["authorization"]);

//         return apiResponse.successResponse(res, msg, []);
//     } catch (err) {
//         return apiResponse.errorResponse(res, err);
//     }
// };

module.exports.delete = async (req, res) => {
    try {
        const employee = await EmployeeModal.findByIdAndDelete(req.params._id);
        const msg = "Employee deleted successfully";

        return apiResponse.successResponse(res, msg, employee);
    } catch (err) {
        return apiResponse.errorResponse(res, err);
    }
};

function getRndInteger(min = 100000, max = 999999) {
    return Math.floor(Math.random() * (max - min)) + min;
}
