const { body, validationResult } = require("express-validator");
const fs = require("fs");
const path = require("path");
const apiResponse = require("../Helpers/ApiResponse");

const validator = () => {
    return [
        body("email")
            .not()
            .isEmpty()
            .withMessage("Email is required")
            .isEmail()
            .withMessage("Email should be in format"),
        body("name")
            .not()
            .isEmpty()
            .withMessage("Name is required")
            .not()
            .isAlphanumeric()
            .withMessage("Name must be character")
            .isLength({ min: 10 })
            .withMessage("Name must contain 10 charater"),
        body("password")
            .not()
            .isEmpty()
            .withMessage("Password is required")
            .isLength({ min: 6 })
            .withMessage("Password must contain 6 charater"),
    ];
};

const messages = (req, res, next) => {
    const validator = validationResult(req);

    if (validator.isEmpty()) {
        return next();
    }

    if (req.file) {
        fs.unlinkSync(path.join(__dirname, "../../", req.file.path));
    }

    return apiResponse.validationError(res, validator.errors[0]["msg"], []);
};

module.exports = {
    validator,
    messages,
};
